#!/bin/sh
# \
exec tclsh "$0" "$@"

set epoch [exec date +%s]
set outpath "/RAID/esbackup"
set outfile esdb_$epoch.tar
set buType $argv


        if { [string length $epoch] != 0 && $buType == "remote"} {
                puts "stopping tomcat"
                catch { exec /home/symlnks/common/WebApplications/launchers/daltomcat-6.0.sh stop }
                puts "tomcat stopped"
                set tomcatStop [exec /bin/ps -ef | grep -i tomcat | wc -l]
    }   elseif  { [string length $epoch] != 0 && $buType == "local"} {
                puts "no need to stop tomcat"
                set tomcatStop 1
    }   else    {
                puts "es database backup could not be started"
                exit 1
    }
        if  { $tomcatStop == 1 } {
                puts "backing up the es database"
                catch { exec /symlnks/DALiM_6.0/script/dalimes_dbtool.sh -backup tar $outpath/$outfile }
    }   else {
                puts "es database backup could not be started"
                exit 1
    }
        if { $buType == "remote" && [file exists $outpath/$outfile] == 1 } {
                puts "starting tomcat"
                catch { exec /home/symlnks/common/WebApplications/launchers/daltomcat-6.0.sh restart }
                set tomcatStart [exec /bin/ps -ef | grep -i tomcat | wc -l]
                puts "es database backup completed sucessfully"
                catch { exec /usr/bin/smbclient //172.16.1.3/Oakville\ Library -A /ioi/bin/.smbclient -c "lcd $outpath; cd /ES3; prompt; recurse; mput $outfile; exit;" }
                puts "backup sent to remote location"
    }   elseif  { $buType == "local" && [file exists $outpath/$outfile] == 1 } {
                puts "es database backup completed sucessfully"
    }   else {
                puts "es database backup failed"
                exit 1
    } 
        if { $buType == "remote" && $tomcatStart == 2 } {
                puts "tomcat started"
                catch { exec /bin/cp -f /ioi/bin/images/pigeonheader.png /home/symlnks/common/tomcat6.0/webapps/Esprit/images/dalimsoftware.png }
                catch { exec /bin/cp -f /ioi/bin/images/pigeonheader.png /home/symlnks/common/tomcat6.0/webapps/Esprit/images/ }
                catch { exec /bin/cp -f /ioi/bin/images/pigeonheadersmall.png /home/symlnks/common/tomcat6.0/webapps/Esprit/images/ }
                puts "pigeon logo restored to main org splash page"
    }   elseif { $buType == "local" } {
                puts "es database backup completed sucessfully"
    }   else {
                puts "tomcat failed to start"
                exit1
    }

exit 0