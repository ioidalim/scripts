#!/bin/sh

## 2017-03-21
## Eric Smith

## Parses machine and Dalim system info for current installed versions and hostiry
## Edit customer name in next line

customer=example
fname=/updates/dalim/$customer`hostname -s``date +%Y%m%d`.xml

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" > $fname
echo "<ServerStats xmlns:feature=\"http:// \" xmlns:history=\"http:// \" xmlns:host=\"http:// \" xmlns:license=\"http:// \" xmlns:OS=\"http://\">" >> $fname
echo "<Host>" >> $fname
echo "<host:Name>"`hostname`"</host:Name>">> $fname
echo "<host:id>"`/symlnks/common/dlm/3.0/bin/dlmutil hostid`"</host:id>" >> $fname
echo "</Host>" >> $fname
echo "<License>" >> $fname
grep License /symlnks/common/dlm/3.0/licenses/license.xml|grep -v DALIM|sed 's/!-- License \(.*\): \(.*\)--/license:\1\>\2\<\/license:\1/' >> $fname
grep "Expire:" /symlnks/common/dlm/3.0/licenses/license.xml|sed 's/<!-- \(.*\): \(.*\)-->/\<license:\1\>\2\<\/license:\1\>/' >> $fname
echo "</License>" >> $fname
echo "<Features>" >> $fname
echo "<feature:HardWorkers>" `grep HWC /symlnks/common/dlm/3.0/licenses/license.xml|cut -d \" -f4`"</feature:HardWorkers>" >> $fname
/symlnks/common/dlm/3.0/bin/dlmutil -l /symlnks/common/dlm/3.0/licenses/license.xml stat|grep Feature|sed 's/Feature \(.*\):\(.*\)/\<feature:\1\>\2\<\/feature:\1\>/' >> $fname
echo "</Features>" >> $fname
echo "<History>" >> $fname
grep HISTORY /symlnks/common/dalimconfig/BUILD_1-0.conf|sed 's/^\(.*\)_HISTORY=\(.*\)/\<history:\1\>\2\<\/history:\1\>/' >> $fname
echo "</History>" >> $fname
echo "<OS>" >> $fname
echo "<OS:Version>"`cat /etc/redhat-release`"</OS:Version>" >> $fname
echo "<OS:Kernel>"`uname -rv`"</OS:Kernel> " >> $fname
echo "</OS>" >> $fname
echo "</ServerStats>" >> $fname
