#!/bin/bash

: <<'endBlockComment'
scriptName: ftpMirrorOSX.sh
version: 1.2
author: Louis D. Monaghan, Jr.
e-mail: lmonaghan@iointegration.com
Coded: 2014-11-05
Last Updated: 2017-04-04
Description: This script is designed to run under OSX and will mirror a FTP servers directory structure using rsync. 

The script will log basic information such a start and stop times and ftp mounting success and errors in a file defined by the mirrorScriptLog variable. Additionally the script will log the output of the rsync command in a file defined by the rsyncLog variable. Both of these log files are replaced everytime the script is run.

The script will process folders to be mirrored based on the mirrorSourceRoot and mirrorList variables. The mirrorSourceRoot variable defines where the script will look for folders defined in the space delimited list mirrorList variable. This allows the user to select which folders within mirrorSourceRoot to synchronise. 

The script is designed to move files that would be deleted by the rsync command to a folder whos location and name are defined by the mirrorDestinationRoot, mirrorList, and archiveDirectoryName variables. Each folder within mirrorSourceRoot and defined by mirrorList placed in mirrorDestinationRoot will contain a folder defined by archiveDirectoryName. The script will backup any changed or removed files from the mirrored FTP server into a dated folder within the arhiveDiretoryName folder. Additionally, rsync will write a dated log in the arhiveDirectoryName folder.

IMPORTANT NOTE: Though the script verifies the FTP server mount location, existence and wether there is a file system mounted, etc. it does NOT verify the destination directory. Make sure your destination is correct and exists.

Change Log:
2014-11-07 - Lou Monaghan - Corrected a date formating error for the command line logging. Modified the rsync operators in two important ways. Removed the -a option and replaced with -rlt due to the file system I write too does not permit the owner, group and permissions to be synced to the source FTP server. The options -rlptgoD are implied by -a, in this case we do not need -pgoD, which are permissions, group, owner, and Devices respectively. If you do not remove the -pgo options rysnc will attempt to fix the permsissions everytime it is run. Also added the --modify-window 3601 option, which allows a 1-hour and 1-second delta between the server and destination. I noticed that files that were synced to my destination when the clocks were set to Daylight time versus Savings time even though they had not changed. This may have more to do with how my destination file systme reports the file times, but it was causing resync of files that was not required. It is unlikely this will cause a problem syncing DALiMs FTP servers, but if you use the script to sync other servers, modify that settings as you need.
2015-09-09 - Eric Smith - Parallelizied rsync calls with function and wait for completion
2017_04-04 - Eric Smith - Added osascript option to pop OSX notification when complete
endBlockComment


#### Set Base Variables ####
	mirrorDateTime=`date +%Y%m%d-%H.%M.%S`
	mirrorScriptLog='~dalimFTPMirrorScriptLog.txt'
	rsyncLog='~dalimFTPRsyncLog.txt'
	archiveLogFile="$mirrorDateTime""_rsyncManifest.txt"
	ftpServer='ftp.dalim.com'
	ftpUser='ioint'
	ftpPassword='New$12Mia'
	ftpMountPoint='/Volumes/dalimFTP'
	mirrorSourceRoot='/'
	mirrorDestinationRoot='/Volumes/Elements/installationFiles/dalimMirror/'
	### Note: mirrorList should be space delimited, and folders must contain a trailing slash ###
	mirrorList='betaproducts/ public/ Sales/'
	archiveDirectoryName='archivedFiles'

	
#### Check to see if mount point exists, if not create, log and exit if not possible. If mount point exists make certain no file systems are mounted, if there are file systems mounted log and exit. ####
	if [ -d $ftpMountPoint ]; then
  	### Mount point found, make certain no file systems are attached to the mount point. ###
  		if mount | grep "$ftpMountPoint" > /dev/null; then
  			echo `date +%Y.%m.%d-%H:%M:%S`" - FTP file system already mounted at $ftpMountPoint. Please unmount any files systems from $ftpMountPoint prior to running the script." > "$mirrorDestinationRoot/$mirrorScriptLog"
  			echo `date +%H:%M:%S`" - FTP file system already mounted at $ftpMountPoint. Please unmount any files systems from $ftpMountPoint prior to running the script." 2>&1
			exit 1
		else
			echo `date +%Y.%m.%d-%H:%M:%S`" - $ftpMountPoint already exists with no file system mounted. No need to create mount point." > "$mirrorDestinationRoot/$mirrorScriptLog"
			echo `date +%H:%M:%S`" - $ftpMountPoint already exists with no file system mounted. No need to create mount point."
		fi
	else
  	### Mount point does not exist, attempt to create, log and exit if not possible. ###
  		mkdir $ftpMountPoint
  		if [ "$?" = "1" ]; then
  			echo `date +%Y.%m.%d-%H:%M:%S`" - $ftpMountPoint could not be created, exiting." > "${mirrorDestinationRoot}${mirrorScriptLog}"
			echo `date +%H:%M:%S`" - $ftpMountPoint could not be created, exiting." > "${mirrorDestinationRoot}${mirrorScriptLog}" 2>&1
			exit 1
		else
			echo `date +%Y.%m.%d-%H:%M:%S`" - Mount point $ftpMountPoint created." > "${mirrorDestinationRoot}${mirrorScriptLog}"
			echo `date +%H:%M:%S`" - Mount point $ftpMountPoint created."
		fi
	fi

#### Attempt to mount FTP file system, log and exit if not possible. (Note: File system will be READ-ONLY) ####
	mount_ftp $ftpUser:$ftpPassword@$ftpServer $ftpMountPoint
	if [ "$?" = "1" ]; then
		echo `date +%Y.%m.%d-%H:%M:%S`" - Could not mount $ftpServer to $ftpMountPoint." >> "$mirrorDestinationRoot/$mirrorScriptLog"
		echo `date +%H:%M:%S`" - Could not mount $ftpServer to $ftpMountPoint." 2>$1
		exit 1
	else
		echo `date +%Y.%m.%d-%H:%M:%S`" - $ftpServer mounted to $ftpMountPoint." >> "$mirrorDestinationRoot/$mirrorScriptLog"
		echo `date +%H:%M:%S`" - $ftpServer mounted to $ftpMountPoint."
	fi
	
##### Loop through folders in mirrorList, rsyncing files as needed and backing up 'deleted' files to archiveDirectory #####
runRsync(){
	i=0
	
	for folder in $mirrorList
	do
		echo `date +%Y.%m.%d-%H:%M:%S`" - Rsync of $ftpServer:$mirrorSourceRoot$folder started." >> "$mirrorDestinationRoot/$mirrorScriptLog"
		echo `date +%H:%M:%S`" - Rsync of $ftpServer:$mirrorSourceRoot$folder started."
		rsync -rltbhP --exclude=Francais --exclude=Français --exclude=Deutsch --exclude=FR --exclude=DE --modify-window 3601 --delete --backup-dir="$mirrorDestinationRoot/$folder/$archiveDirectoryName/$mirrorDateTime/" --exclude="$archiveDirectoryName" --log-file="$mirrorDestinationRoot$folder$archiveDirectoryName/$archiveLogFile" "$ftpMountPoint$mirrorSourceRoot$folder" "$mirrorDestinationRoot/$folder" > "$mirrorDestinationRoot/$rsyncLog" 2>&1 &
		pids[$i]=$!
		i=$((i+1))
		#echo `date +%Y.%m.%d-%H:%M:%S`" - Rsync of $ftpServer:$mirrorSourceRoot$folder completed." >> "$mirrorDestinationRoot/$mirrorScriptLog"
		#echo `date +%H:%M:%S`" - Rsync of $ftpServer:$mirrorSourceRoot$folder completed."
	done
	
	for pid in ${!pids[*]};
	do
		wait ${pids[$pid]}
	done
}

runRsync

 	
		echo `date +%Y.%m.%d-%H:%M:%S`" - Rsync of $ftpServer completed." >> "$mirrorDestinationRoot/$mirrorScriptLog"
		echo `date +%H:%M:%S`" - Rsync of $ftpServer completed."

	
	chmod -R 777 $mirrorDestinationRoot
	
##### Attempt to Unmount FTP file system and notify user that the mirror is complete #####
	### Pause 5 seconds then attempt to unmount FTP file system ###
	sleep 5
	umount $ftpMountPoint
	if [ "$?" = "1" ]; then
  		echo `date +%Y.%m.%d-%H:%M:%S`" - $ftpMountPoint could not be unmounted." >> "$mirrorDestinationRoot/$mirrorScriptLog"
  		echo `date +%H:%M:%S`" - WARNING: $ftpMountPoint could not be unmounted." 2>&1
  		echo `date +%Y.%m.%d-%H:%M:%S`" - Mirror Complete." >> "$mirrorDestinationRoot/$mirrorScriptLog"
		echo `date +%H:%M:%S`" - Mirror complete."
                osascript -e 'display notification "Dalim Mirror Complete" with title "FTP" sound name "glass”'
		exit 1
	else
		echo `date +%Y.%m.%d-%H:%M:%S`" - $ftpMountPoint unmounted." >> "$mirrorDestinationRoot/$mirrorScriptLog"
		echo `date +%H:%M:%S`" - $ftpMountPoint unmounted."
		echo `date +%Y.%m.%d-%H:%M:%S`" - Mirror Complete." >> "$mirrorDestinationRoot/$mirrorScriptLog"
		echo `date +%H:%M:%S`" - Mirror complete."
                 osascript -e 'display notification "Dalim Mirror Complete" with title "FTP" sound name "glass"""'
	fi

