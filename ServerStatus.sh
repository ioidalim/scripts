#!/bin/sh

## 2017-03-21
## Eric Smith

## Parses machine and Dalim system info for current installed versions and hostiry

fname=/updates/dalim/`hostname -s``date +%Y%m%d`

echo "Host:" > $fname.txt
echo "Name:" `hostname` >> $fname.txt
echo "ID:" `/symlnks/common/dlm/3.0/bin/dlmutil hostid` >> $fname.txt
echo "" >> $fname.txt
echo "License:" >> $fname.txt
grep License /symlnks/common/dlm/3.0/licenses/license.xml|grep -v DALIM|sed 's/<!-- License //'|sed 's/-->//' >> $fname.txt
grep "Expire:" /symlnks/common/dlm/3.0/licenses/license.xml|sed 's/<!-- //'|sed 's/-->//' >> $fname.txt >> $fname.txt
echo "" >> $fname.txt
echo "Features:" >> $fname.txt
echo "HardWorkers:" `grep HWC /symlnks/common/dlm/3.0/licenses/license.xml|cut -d \" -f4` >> $fname.txt
/symlnks/common/dlm/3.0/bin/dlmutil -l /symlnks/common/dlm/3.0/licenses/license.xml stat|grep Feature|sed 's/Feature //' >> $fname.txt
echo "" >> $fname.txt
echo "History:" >> $fname.txt
grep HISTORY /symlnks/common/dalimconfig/BUILD_1-0.conf|sed 's/_HISTORY=/: /' >> $fname.txt
echo "" >> $fname.txt
echo "OS:" >> $fname.txt
echo "Version: "`cat /etc/redhat-release` >> $fname.txt
echo "Kernel: " `uname -rv` >> $fname.txt
