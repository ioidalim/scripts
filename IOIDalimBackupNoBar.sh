#!/bin/sh
#v5 061515

## Full backup of all Dalim files and system settings
## Does NOT include jobs directory for Twist
## Does include ES internal file locations but not live database or Full Text Index 
##
## To save the current ES db, run dalimes_dbtool.sh before running this script
##
## Requires installation of bar-1.11.1-2.x86_64.rpm
##
## To restore, reinstall the base product and untar the archives
##
## This can be a very sizable backup. Check disk space before starting.

filedir=`date +%Y%m%d-%s`
budir=/updates/dalim/backups
mkdir -p ${budir}/${filedir}

grep ES /symlnks/common/dalimconfig/BUILD_1-0.conf  2>&1 > /dev/null
isES=`echo $?`

grep TWIST /symlnks/common/dalimconfig/BUILD_1-0.conf  2>&1 > /dev/null
isTWIST=`echo $?`

if [ ${isES} -eq "0" ]
then
  echo "Backing up db"
  /home/symlnks/DALiM_6.0/script/dalimes_dbtool.sh -backup compress ${budir}/${filedir}/ESdb-ioi.backup
fi

if [ ${isTWIST} -eq "0" ] && [ ${isES} -eq "1" ]
then
echo "Backing up Twist /symlnks directory..."
tar czpP --one-file-system -f ${budir}/${filedir}/symlnks.tgz /home/symlnks --exclude=/home/symlnks/io/jobs --exclude=/home/symlnks/process/6.0 --exclude=/home/symlnks/io/output --exclude=/home/symlnks/notes 
fi

if [ ${isTWIST} -eq "0" ] && [ ${isES} -eq "0" ]
then
echo "Backing up Twist and ES /symlnks directory..."
tar czpP --one-file-system -f ${budir}/${filedir}/symlnks.tgz /home/symlnks --exclude=/home/symlnks/io/jobs --exclude=/home/symlnks/process/6.0 --exclude=/home/symlnks/data/es --exclude=/home/symlnks/var/es/database --exclude=/home/symlnks/var/es/esFulltextIndex --exclude=/home/symlnks/var/es/ESPRiTQueue --exclude=/home/symlnks/io/output --exclude=/home/symlnks/notes
fi

if [ ${isES} -eq "0" ] && [ ${isTWIST} -eq "1" ]
then
  echo "Backing up ES /symlnks directory..."
  tar czpP --one-file-system -f ${budir}/${filedir}/symlnks.tgz /home/symlnks --exclude=/home/symlnks/data/es --exclude=/home/symlnks/var/es/database --exclude=/home/symlnks/var/es/esFulltextIndex --exclude=/home/symlnks/var/es/ESPRiTQueue 
fi

echo "Backing up rpm directory..."
tar czpP --one-file-system -f ${budir}/${filedir}/rpm.tgz /var/lib/rpm 
echo "Backing up /etc..."
tar czpP --one-file-system -f ${budir}/${filedir}/etc.tgz /etc 