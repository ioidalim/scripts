#!/bin/bash

: <<'endBlockComment'
scriptName: TwistRsync.sh
version: 1.0.1
author: Eric Smith (via Ryan From)
e-mail: eric.smith@iointegration.com
Coded: 2015-02-17
Last Updated: 2018-04-13
Description: This script is designed to run under linux and will synchronize Twist configuration folders from a 
remote source to a local target. No verification of remote or local content. Caveat Emptor.

endBlockComment

.#### Set Base Variables ####
	user='root'				# remote user
	server='192.168.0.1'	# remote source server
	oldtwistname='Twist7vm' # Name of old Twist server
	newtwistname='diogenes' # Name of new twist server
	
rsync -arv ${user}@${server}:/home/symlnks/var/6.0/TWiST/repository/ouroot/workflows /home/temp/
/bin/ls -1 /home/temp/workflows/ > files.lst
for file in $(cat files.lst);do;sed 's/$oldtwistname/$newtwistname/g' $file > /home/symlnks/var/6.0/TWiST/repository/ouroot/workflows/$file > ;done
chmod 664 /home/symlnks/var/6.0/TWiST/repository/ouroot/workflows/*
chown twistadm /home/symlnks/var/6.0/TWiST/repository/ouroot/workflows/*
chgrp dalim /home/symlnks/var/6.0/TWiST/repository/ouroot/workflows/*
rsync -arcv ${user}@${server}:/home/symlnks/var/ppd/*.ppd /home/symlnks/var/ppd/
rsync -arcv ${user}@${server}:/home/symlnks/var/ppd/*.PPD /home/symlnks/var/ppd/
rsync -arcv ${user}@${server}:/home/symlnks/var/6.0/colors/cco/* /home/symlnks/var/6.0/colors/cco/
rsync -arcv ${user}@${server}:/home/symlnks/var/6.0/colors/channelGradation/* /home/symlnks/var/6.0/colors/channelGradation/
rsync -arcv ${user}@${server}:/home/symlnks/var/6.0/colors/pantone/* /home/symlnks/var/6.0/colors/pantone/
rsync -arcv ${user}@${server}:/home/symlnks/setup/publishOnMac.cfg /home/symlnks/setup/
rsync -arcv ${user}@${server}:/home/symlnks/setup/TWiST/CustomMetadata.xml /home/symlnks/setup/TWiST/
rsync -arcv ${user}@${server}:/home/symlnks/setup/TWiST/DbConfig.cnf /home/symlnks/setup/TWiST/
rsync -arcv ${user}@${server}:/home/symlnks/setup/TWiST/Default_DbConfig* /home/symlnks/setup/TWiST/
rsync -arcv ${user}@${server}:/home/symlnks/setup/TWiST/Expressions.cnf /home/symlnks/setup/TWiST/
rsync -arcv ${user}@${server}:/home/symlnks/setup/TWiST/JdbcDrivers/mysql* /home/symlnks/setup/TWiST/JdbcDrivers/
rsync -arcv ${user}@${server}:/home/symlnks/setup/TWiST/JdbcDrivers/*.jar /home/symlnks/setup/TWiST/JdbcDrivers/
rsync -arcv ${user}@${server}:/home/symlnks/setup/TWiST/Tools /home/symlnks/setup/TWiST/
rsync -arcv ${user}@${server}:/home/symlnks/var/6.0/icc/* /home/symlnks/var/6.0/icc/
rsync -arcv ${user}@${server}:/home/symlnks/var/6.0/Directory/derbydb /home/symlnks/var/6.0/Directory/
rsync -arcv ${user}@${server}:/home/symlnks/DALiM_6.0/iowfl/io /home/symlnks/DALiM_6.0/iowfl/ 